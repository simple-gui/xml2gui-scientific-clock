
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')
import xml2gui as g

    
def listener_drag(id, msg):
    print(f"DRAG    id: {id}, msg: {msg}")
    if id == "FlatEarth":
        list = msg.split("|")
        g.mcxml_set_left(id, list[1])
        g.mcxml_set_top(id, list[2])
        g.mcxml_set()


if __name__ == '__main__':
    xml = open('app.xml', mode='r', encoding='utf-8').read()
    g.mcxml_listener_drag(listener_drag)
    g.mcxml_loop(xml)
    
